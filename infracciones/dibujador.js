function createTable(infracciones) {
  var div = $('#table_div');
  var table = $('<table></table>');
  var tBody = $('<tbody></tbody>')
  var columnas = ['Nro Infracción',
                  'Monto',
                  'Fecha de Registro',
                  'Dirección Registrada',
                  'Tipo de Infracción',
                  'Ubicación',
                  'Pagar'];
  var hilera = $('<tr></tr>');

  columnas.forEach(function(columna){
    var celda = $('<td></td>');
    var textoCelda = document.createTextNode(columna);
    celda.append(textoCelda);
    hilera.append(celda);
    hilera.css('fontWeight','bold');
    tBody.append(hilera);
  });

  infracciones.forEach(function(infraccion){
    var hileraAux = $('<tr></tr>');
    var celdaNro = $('<td></td>');
    var celdaMonto = $('<td></td>');
    var celdaRegistro = $('<td></td>');
    var celdaDireccion = $('<td></td>');
    var celdaTipo = $('<td></td>');
    var celdaUbicacion = $('<td></td>');
    var celdaPagar = $('<td></td>');

    var textoNro = document.createTextNode(infraccion.id);
    var textoMonto = document.createTextNode(infraccion.montoAPagar);
    var textoRegistro = document.createTextNode(infraccion.fechaHoraRegistro);
    var textoDireccion = document.createTextNode(infraccion.direccionRegistrada);
    var textoUbicacion = $('<a></a>');
    var textoUbicacionIcono = $('<i></i>');
    var textoPagar = $('<input></input>');

    textoUbicacionIcono.attr("class", "fa fa-paper-plane");
    textoUbicacion.css('color', '#444');
    textoUbicacion.attr("href", "../mapa_depositos/mapa_depositos.html");
    textoUbicacion.attr('id', 'link');
    textoUbicacion.append(textoUbicacionIcono);

    textoPagar.attr("type", "checkbox");

    celdaNro.append(textoNro);
    celdaMonto.append(textoMonto);
    celdaRegistro.append(textoRegistro);
    celdaDireccion.append(textoDireccion);
    
    //Se hace append del tipo de infraccion obtenido por el fetch a la api
    addTipoInfraccionCelda(celdaTipo, infraccion.tipoInfraccion);

    // Si existe acarreo, hago el appendChild del icono, sino hago appendChild de un parrafo vacio
    if(infraccion.existeAcarreo){
      celdaUbicacion.append(textoUbicacion);
      textoUbicacion.css("margin-left", "25px");
      getAcarreoId(infraccion.patente, infraccion.id);
    }
    else{
      celdaUbicacion.append($('<p></p>'));
    }

    celdaPagar.append(textoPagar);

    hileraAux.append(celdaNro);
    hileraAux.append(celdaMonto);
    hileraAux.append(celdaRegistro);
    hileraAux.append(celdaDireccion);
    hileraAux.append(celdaTipo);
          
    // Si existe acarreo, hago el appendChild de la celda, sino hago appendChild de un parrafo vacio
    if(infraccion.existeAcarreo){
      hileraAux.append(celdaUbicacion);
    }
    else{
      hileraAux.append($('<p></p>')); 
    }

    hileraAux.append(celdaPagar);
    tBody.append(hileraAux);  
  });

  var btn = $('<button></button>');
  btn.attr("class", "button");
  btn.text('Abonar');

  table.append(tBody);
  div.append(table);
  div.append(btn);
  table.attr('class', 'table');
}

function addTipoInfraccionCelda(celda, tipoID){
  var endPointTipoInfraccion = 'tiposInfraccion/' + tipoID;

  //Se hace el fetch del tipo de infraccion para mostrar
  fetch(urlAPI  + endPointTipoInfraccion)
    .then((response) => {
      response.json()
    .then((data) => {
      celda.append(document.createTextNode(data.tipo.descripcion));
    });
  });
}

function verifyRole() {
  var role = getRoleFromURL(window.location.href);

  if(role) {
    $("#home_link").attr('href', '../index/index.html?role=' + role);
  }
}