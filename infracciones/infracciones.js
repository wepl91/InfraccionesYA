
var urlAPI = 'https://infraccionesya.herokuapp.com/api/';

$(function(){
  function searchInfraction() {
    var patente = $('#input_patente').val().toUpperCase(); 
    var endPointInfracciones = patente + '/infracciones/';

    $('#table_div').empty();

    if(!patente){
      alert('Debe ingresar una patente para realizar la búsqueda');
    }
    getInfractions(urlAPI + endPointInfracciones);

    $('#input_patente').val('');
  }
  
  $('#search_button').on('click', function(){
    searchInfraction();
  });
});

function getInfractions(url) {
  fetch(url)
    .then((response) => { 
      response.json()
    .then((data) => {
      if(!data.infracciones.length){
        alert('Este auto no posee infracciones');
      } 
      else{
        createTable(data.infracciones);  
      }
    });
  });
}

function getAcarreoId(patente, infraccionId) {
  var endPointAcarreoUbicacion = patente + '/acarreos/' + infraccionId;
  
  //Se hace el fetch de la ubicacion del acarreo
  fetch(urlAPI + endPointAcarreoUbicacion).then(function(response) {
    response.json()
    .then(function(data){
      $('#link').attr("href", "../mapa_depositos/mapa_depositos.html" + getRoleFromURL(window.location.href) + "&id=" + data.acarreo.deposito.id);
    });
  });
}

function getRoleFromURL(url) {
  return url.split('role=')[1] ? '?role=' + url.split('role=')[1] : '?'
}
