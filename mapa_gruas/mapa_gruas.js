function bootstrap(map) {
  var role = getRoleFromURL(window.location.href);
	var url = "https://infraccionesya.herokuapp.com/api";
	var urlGruas = '/gruas';
	var urlEstados = '/estadosGruas';
  var urlPosiciones = '/posiciones';
  var map = L.map('map', { 
                center: [-34.521768691219684, -58.70119161424685],
                minZoom: 2,
                zoom: 13
               });
  	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
              subdomains: ['a','b','c']
             })
      .addTo(map);
    var colors = ['blue', 'red', 'green', 'orange', 'violet', 'grey']; 

  	/*Funciones de fetch*/

    var traerGruas = function() {
      return fetch(url + urlGruas)
              .then((response) => response.json())
    }

    var traerPosiciones = function (grua) {
      return fetch(url + urlGruas + "/" + grua.id + urlPosiciones)
              .then((response) => response.json())
              .then(function(responseJSON){
                grua.posiciones = new Array()

                var posicionInicial = {             //cambio la ubicacion inicial al formato
                  ubicacion: grua.ubicacion,       //de posicion usado en la API de posiciones
                  estado: grua.estado_id};

                responseJSON.posiciones.unshift(posicionInicial)  //agrego la posicion inicial
                grua.posiciones = responseJSON.posiciones     //al resto de posiciones de la API

                delete grua.ubicacion //borro datos que ya no se usan mas
                delete grua.estado_id
                
                return grua;                
              });
    }

    var traerEstado = function(posicion) {
      return fetch(url + urlEstados + "/" + posicion.estado)
              .then((response) => response.json())
              .then(function(responseJSON) {
                posicion.estado = responseJSON.estado.descripcion;
                return posicion
          })
    }
 	
  var dibujarGruas = function() {
  		
  	traerGruas()
      .then((response) => response.gruas.forEach((grua) => {
        var color = colors.pop()
        setColorHeader(color,grua.id)
        traerPosiciones(grua)
      .then(Dibujar(grua,map,color))
      }))
      
	}
    
  dibujarGruas();

  if(role) {
    $("#home_link").attr('href', '../index/index.html?role=' + role);
  }
}

function getRoleFromURL(url) {
  return url.split('role=')[1];
}