var Dibujar = function(grua, map, color) {
	var url = "https://infraccionesya.herokuapp.com/api";
	var urlEstados = '/estadosGruas';
	var marker = null
	var id = grua.id

	function traducirEstado(id) {
		return fetch(url + urlEstados + "/" + id)
				.then((response) => response.json())
				.then(function(responseJSON) {
					return responseJSON.estado.descripcion
				});
	}

	function redibuja(coordenada) {
		var info = traducirEstado(coordenada.estado)
		var pos = coordenada.ubicacion
		if(!marker) {

			var urlIcon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-' + color + '.png';
			var icon = new L.Icon({
  			iconUrl: urlIcon,
  			shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  			iconSize: [25, 41],
  			iconAnchor: [12, 41],
  			popupAnchor: [1, -34],
  			shadowSize: [41, 41]
			});

			marker = L.marker(pos, {icon: icon})
                .addTo(map);
		}

		info.then((response) => {
			marker.bindPopup("Grua:" + id + "<br/>" + response.substring(0, response.length -1));
			$('#status_grua_' + id).text(response.substring(0, response.length -1))
		});

		/*info.then((response)=> marker.bindPopup("Grua:"+id+"<br/>"+response.substring(0, response.length -1))
			$('#status_grua_' + id).text(response.substring(0, response.length -1)));*/

			//"estado:"+response))
		marker.setLatLng(pos);
	}

	function update() {
		if(grua.posiciones.length) {
			redibuja(grua.posiciones.shift());
			setTimeout(update, 1000);
		}
	}

	setTimeout(update, 1000);
}

function setColorHeader(color, id) {
  switch(color) {
    case 'violet':
      $('#header_id_' + id).text('Grúa violeta');
      break;
    case 'grey':
      $('#header_id_' + id).text('Grúa gris');
      break;
    case 'orange':
      $('#header_id_' + id).text('Grúa naranja');
      break;
    case 'green':
      $('#header_id_' + id).text('Grúa verde');
      break;
    case 'red':
      $('#header_id_' + id).text('Grúa roja');
      break;
    case 'blue':
      $('#header_id_' + id).text('Grúa azul');
      break;
  }
}