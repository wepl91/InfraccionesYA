function drawMapWithDeposits(map){
  var urlAPI = 'https://infraccionesya.herokuapp.com/api';
  var endPointDepositos = '/depositos';
  var map = L.map('map', { 
    center: [-34.541765, -58.7160749],
    minZoom: 2,
    zoom: 15
  });
  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a','b','c']
  }).addTo( map );

  var traerDepositos = function() {
    return fetch(urlAPI + endPointDepositos)
            .then((response) => response.json()); 
  }
  
  traerDepositos()
    .then((response) => response.depositos.forEach((deposito) => {
      dibujar(deposito, map);
  }));
}