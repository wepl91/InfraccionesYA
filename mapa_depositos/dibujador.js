var dibujar = function(deposito, map) {
  var id = getIdFromURL(window.location.href);
  var role = getRoleFromURL(window.location.href);
  var popUpContent = 'Depósito: ' + deposito.nombre + 
                     '<br>' + 
                     'Dirección: ' + deposito.direccion + 
                     '<br>' + 
                     'Horarios: ' + deposito.horarios + 
                     '<br>' + 
                     'Teléfono: ' + deposito.telefono;
  if(id) {
    if(deposito.id == id){
      L.marker([deposito.ubicacion.lat, deposito.ubicacion.lon])
       .addTo(map)
       .bindPopup('<h3>Tu vehículo se encuentra aquí!</h3>' + popUpContent)
       .openPopup();
    }
  }
  else {
    L.marker([deposito.ubicacion.lat, deposito.ubicacion.lon])
     .addTo(map)
     .bindPopup(popUpContent)
     .openPopup();
  }

  fillInformationList(deposito.id, deposito.nombre, deposito.direccion, deposito.horarios, deposito.telefono);

  if(role) {
    $("#home_link").attr('href', '../index/index.html?role=' + role);
  }
}

function getIdFromURL(url) {
  return url.split('id=')[1];
}

function getRoleFromURL(url) {
  if(url.split('role=')[1]){
    return url.split('role=')[1].split('&')[0];
  }
}

function fillInformationList(id, nombre, direccion, horarios, telefono) {
  $("#depo" + id + "_name").text("Depósito: " + nombre);
  $("#depo" + id + "_dir") .text("Dirección: " + direccion);
  $("#depo" + id + "_hor") .text("Horarios: " + horarios);
  $("#depo" + id + "_tel") .text("Teléfono: " + telefono);
}