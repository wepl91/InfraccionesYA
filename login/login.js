
function validate_inputs(user_input, password_input, depositEmployee) {  
  console.log(depositEmployee)
  if(user_input == '') {
    alert("Debe ingresar un usuario para iniciar seción.");
    return false;
  }

  if(password_input == '') {
    alert("Debe ingresar una contraseña para iniciar seción.");
    return false;
  }
  return true;
}

function login() {
  var user_input = $("#user_input").val().toUpperCase();
  var password_input = $("#password_input").val();
  var depositEmployee = {
    name    : 'ADMIN',
    password: 'admin',
    type    : 'deposito'
  }
  
  var user_loged = {
    name: '',
    type: ''
  };

  if(validate_inputs(user_input, password_input, depositEmployee)) {
    if(depositEmployee.name === user_input && depositEmployee.password === password_input){
      user_loged.name = depositEmployee.name
      user_loged.type = depositEmployee.type
      user_loged.password = depositEmployee.password
    } 
    else if(depositEmployee.name !== user_input || depositEmployee.password !== password_input) {
      alert("Contraseña o usuario inexistente");
      return;
    }
    
    $(document).ready(function() {
      var url = '../index/index.html?role=' + user_loged.type;
      $(location).attr('href', url);
    });
  }
}

$(function(){
  function animateLogin(){
    $('form').animate({height: "toggle", opacity: "toggle"},"slow");
  }
  $('.message a').click(function(){
    animateLogin();    
  });
});