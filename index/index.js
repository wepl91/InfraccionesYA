function verifyRole() {
  var role = getRoleFromURL(window.location.href);
  
  if(role && role != 'deposito') {
    $("#cranes_box").click(function(e){
      e.preventDefault();
      alert("Solo pueden acceder a esta opción los encargados de los depósitos");
    });
  }
  
  if(role) {
    $("#newsletter").hide();
    $("#infraction_link").attr('href', '../infracciones/infracciones.html?role=' + role);
    $("#deposit_link").attr('href', '../mapa_depositos/mapa_depositos.html?role=' + role);
    $("#crane_link").attr('href', '../mapa_gruas/mapa_gruas.html?role=' + role);
  }
  
  if(!role) {
    $("#notifications").hide();
    $("#logout_button").hide();
    $("#cranes_box").click(function(e){
      e.preventDefault();
      alert("Solo pueden acceder a esta opción los encargados de los depósitos");
    });
  }
}

function getRoleFromURL(url) {
  return url.split('role=')[1];
}

$(document).ready(function() {
  var url = '../index/index.html';
  $('#logout_button').on('click', function(){
    $(location).attr('href', url);
  });
  
});